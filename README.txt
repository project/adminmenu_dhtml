adminmenu_dhtml.module
README.txt

Description
------------

Adminmenu_dhtml renders a selected menu as a DHTML menu. This module makes it possible to provide a drop down or other style admin menu that is more convenient for administrators and won�t interfere with a website�s theme.

Select the menu to be rendered by picking a menu from the �Parent menu� dropdown in the adminmenu_dhtml config. This can be any menu and defaults to the Navigation menu. The menu items for your chosen menu do NOT need to be expanded to make this module work correctly.

Viewing of the DHTML menu is controlled by an access control permission labeled �view menu� in the adminmenu_dhtml section.

If you have the devel module installed, and check �Include Devel menu items� in the modules configuration settings, useful devel module menu items will show up in the DHTML menu in addition to the items that exist in the configured menu.

To install a new DHTML menu template, create a new directory in the templates directory located in the admininment_dhtml module directory. Place all .js and .css files in this directory. Create a text file called info.txt and place it in that directory. The first line of this file should contain a human readable description of the template. This description will show up in the dropdown �Menu Type� setting in the adminmenu_dhtml config page. Additional lines in the info.txt file should contain filenames of .js or .css files. No path is required for these files if they exist in the same directory as info.txt. You may add as many .js or .css files as required and they will all automatically be loaded when the DHTML menu is rendered.
