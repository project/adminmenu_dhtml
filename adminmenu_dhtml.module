<?php

/**
 * @file
 * Renders a selected menu as a DHTML menu.
 * This module makes it possible to provide a drop down or other style admin menu that is 
 * more convenient for administrators and won�t interfere with a website�s theme. The menu 
 * style can be customized and turned on for only those user�s that required its functionality. 
 */

/**
 * Implementation of hook_help().
 */
function adminmenu_dhtml_help($section) {
  switch ($section) {
    case 'admin/help#adminmenu_dhtml':
      return t('<p>Adminmenu_dhtml renders a selected menu as a DHTML menu. This module makes it possible to provide a drop down or other style admin menu that is more convenient for administrators and won�t interfere with a website�s theme.</p>

<p>Select the menu to be rendered by picking a menu from the �Parent menu� dropdown in the adminmenu_dhtml config. This can be any menu and defaults to the Navigation menu. The menu items for your chosen menu do NOT need to be expanded to make this module work correctly.</p>

<p>Viewing of the DHTML menu is controlled by an access control permission labeled �view menu� in the adminmenu_dhtml section.</p>

<p>If you have the devel module installed, and check �Include Devel menu items� in the modules configuration settings, useful devel module menu items will show up in the DHTML menu in addition to the items that exist in the configured menu.</p>

<p>To install a new DHTML menu template, create a new directory in the templates directory located in the admininment_dhtml module directory. Place all .js and .css files in this directory. Create a text file called info.txt and place it in that directory. The first line of this file should contain a human readable description of the template. This description will show up in the dropdown �Menu Type� setting in the adminmenu_dhtml config page. Additional lines in the info.txt file should contain filenames of .js or .css files. No path is required for these files if they exist in the same directory as info.txt. You may add as many .js or .css files as required and they will all automatically be loaded when the DHTML menu is rendered.</p>
<p>You can</p>
<ul>
<li>Configure Adminmenu_dhtml at <a href="/admin/settings/adminmenu_dhtml">administer >> settings >> adminmenu_dhtml</a>.</li>
<li>Set Access Control Permissions at <a href="/admin/access">administer >> access control</a>.</li></ul>');
    case 'admin/modules#description':
      return t('Renders the navigation menu using a selected DHTML template.');
  }
}


/**
 * Implementation of hook_settings().
 */

function adminmenu_dhtml_settings() {
	 $form['adminmenu_dhtml_default'] = array(
    '#type' => 'fieldset',
    '#title' => t('adminmenu_dhtml settings'));
	
    $options = menu_parent_options($item['mid'], 0);

    $form['adminmenu_dhtml_default']['adminmenu_dhtml_pid'] = array('#type' => 'select',
      '#title' => t('Parent menu'),
      '#default_value' => variable_get('adminmenu_dhtml_pid', 1),
      '#options' => $options,
      '#description' => 'The menu that will be displayed using the selected DHTML template.'
    );
    $menu_types=_adminmenu_dhtml_menu_types();   
   $form['adminmenu_dhtml_default']['adminmenu_dhtml_menu_type'] = array('#type' => 'select',
     '#title' => t('Menu Type'),
     '#default_value' => variable_get('adminmenu_dhtml_menu_type', _adminmenu_dhtml_default_type()),
     '#options' => $menu_types,
     '#description' => 'The menu type to render. See help for instructions on adding new menu types.'
    );
    $form['adminmenu_dhtml_default']['adminmenu_dhtml_devel'] = array('#type' => 'checkbox',
      '#title' => t('Include Devel menu items'),
      '#default_value' => variable_get('adminmenu_dhtml_devel', 1),
      '#description' => 'Include menu items from the Devel module, if installed.'
    );
 

  return $form;
}
/**
 * Returns an option array containing possible adminmenu styles. 
 * Searches all directories in the module's root directory for an info.txt file,
 * describing available dhtml menu templates.
 * 
 * See the help section for an explanation of the format of info.txt
 *
 */

function _adminmenu_dhtml_menu_types($get_default=0) {
  $options=array();
  $dir=file_scan_directory(drupal_get_path('module', 'adminmenu_dhtml'), 'info.txt',array('.', '..', 'CVS'),0,TRUE);
  foreach ($dir as $file=>$file_info) {
    // I really miss Perl's file handling!	
    $fh = fopen($file, 'r') or die("Can't open $dir[$template]['filename']");
    $path=rtrim($file,'info.txt');
    $description=fgets($fh);
    $value='';
    while (!feof($fh)) {
      $value.=rtrim($path.rtrim(fgets($fh))," ").',';
    }
    // I really miss Perl's chop()!
    $value=rtrim($value,',');
    $options[$value]=$description;	  
    fclose($fh);
    if ($get_default) {
      break;
	}
  }
  return $options;
}
/**
 * For first time installations, we don't know what
 * to default the DHTML menu type to. 
 * 
 * This function finds the first template described by info.txt
 * in the module directory and returns it as the default value.
 */
function _adminmenu_dhtml_default_type() {
  $current=variable_get('adminmenu_dhtml_menu_type',0);
  // to avoid reading the directory every time, we look for a set
  // value. If none is found, we read the directory and set a value
  // to prevent a performance hit the next time this function is called.
  if (!$current) {
    $first_item=each(_adminmenu_dhtml_menu_types(1));
    variable_set('adminmenu_dhtml_menu_type',$first_item[0]);
    return $first_item[0];
  } else {
  	return $current;
  }
}

/**
 * Generates selected menu tree recursively.
 * Eliminates the need to have a menu fully expanded.
 */

function adminmenu_dhtml_menu_tree($pid=1,$as_xml=0) {
  if ($as_xml) {
    // For AJAX enabled menus, we may not know the correct menu path so we must pass it in
    $path=check_url($_GET['path']);
    menu_set_active_item($path);
  }
  $menu = menu_get_menu();
  $output = ''; 
  if (isset($menu['visible'][$pid]) && $menu['visible'][$pid]['children']) {
    foreach ($menu['visible'][$pid]['children'] as $mid) {
      // Build 
      
      if (count($menu['visible'][$mid]['children']) > 0) {
	    if ($as_xml) {
  		  $type = isset($menu['visible'][$mid]['type']) ? $menu['visible'][$mid]['type'] : NULL; 
		  if (menu_in_active_trail($mid) || ($type & MENU_EXPANDED)) {
		    $open='"TRUE"';
		  } else {
		    $open='"FALSE"';
		  }
		  $output .= '<item open='.$open.'>'.menu_item_link($mid).adminmenu_dhtml_menu_tree($mid,$as_xml).'</item>';
		} else {
          $output .= '<li class="expanded">'.menu_item_link($mid).'<ul class="menu">'.adminmenu_dhtml_menu_tree($mid,$as_xml).'</ul></li>';
		}
      } 
      else {
	    if ($as_xml) {
		  $output .= '<item class="leaf" open="TRUE">'.menu_item_link($mid).'</item>';
		} else {
          $output .= "<li class='leaf'>".menu_item_link($mid)."</li>";
		}
      }
    }
  }
  return $output;
}

function adminmenu_dhtml_get_menu_xml() {
  $mid=variable_get('adminmenu_dhtml_pid', 1);
  $parent_menu=menu_get_item($mid);
  echo '<menu title="'.$parent_menu['title'].'">'.adminmenu_dhtml_menu_tree($mid,1).'</menu>';
}

function adminmenu_dhtml_footer() {
  if (user_access('view menu')) {    
    return adminmenu_dhtml_build_menu();
  }       
}


/**
 * Implementation of hook_menu().
 */

function adminmenu_dhtml_menu($may_cache) {
  $items[] = array(
    'path' => 'adminmenu_dhtml/xml_menu',
    'access' => user_access('view menu'),
    'callback' => 'adminmenu_dhtml_get_menu_xml',
	'type' => 'MENU_CALLBACK');
	
	if (!$may_cache && user_access('view menu')) {
  	$includes=explode(',',variable_get('adminmenu_dhtml_menu_type',_adminmenu_dhtml_default_type()));
	$add_once=1;
	$ajax=0;
  	 for($i=0; $i < count($includes); $i++) {
  	 	/** We run a check_url as an added measure of security since this value initially
  	 	* comes from a selection option in the adminmenu_dhtml settings menu.
  	 	* This is most likely overkill.
  	 	**/
  	 	$clean=check_url($includes[$i]);
  	 	// I miss Perl regex $x =~ /ddd/;!
  	 	/**
  	 	 * Here we add the javascript file(s) and style sheet(s)
  	 	 */
  	 	if (preg_match('/\.js/',$clean)) {
		    if ($add_once) {
			  drupal_set_html_head("<script type='text/javascript'>var AdminmenuPath='".substr($clean,0,strrpos($clean,'/'))."'</script>");
			  $add_once=0;
			}
  	 		drupal_add_js($clean);
  	 	} elseif (preg_match('/\.css$/',$clean)) {
  	 		theme_add_style($clean);
  	 	} 
		if (preg_match('/AJAX/',$clean)) {
		  $ajax=1;
		}
  	 }
	 if ($ajax) {
	   variable_set('adminmenu_dhtml_ajax',1);
	 } else {
       variable_set('adminmenu_dhtml_ajax',0);
	 }
  }	
 return $items;
 
}

/**
 * Build the administrative menu shown to admins
 *
 * @return
 *  A string of HTML representing the admin menu
 */
function adminmenu_dhtml_build_menu() {
  if (!variable_get('adminmenu_dhtml_ajax', 1)) {
    $output = '<ul id="admin-nav">';
     if (module_exist('devel') && variable_get('adminmenu_dhtml_devel', 1)) {
      $output .= '<li class="expanded"><a href="#">devel</a><ul class="menu">';
      $output .= '<li class="leaf"><a href="'. url('admin/settings/devel') .'">module settings</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/cache/clear') .'">empty cache</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/phpinfo') .'">phpinfo()</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/menu/reset') .'">reset menus</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/reinstall') .'">reinstall modules</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/variable') .'">variable viewer</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/execute') .'">execute php code</a></li>';
      $output .= '<li class="leaf"><a href="'. url('devel/node_access/summary') .'">node access summary</a></li>';
      $output .= '</ul></li>';
    }
    $output .= adminmenu_dhtml_menu_tree(variable_get('adminmenu_dhtml_pid', 1));
    $output .= '</ul>';
  
    return $output;
  }	
}


/**
 * Implementation of hook_perm().
 */
function adminmenu_dhtml_perm() {
  return array('view menu');
}