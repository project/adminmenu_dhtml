--- README  -------------------------------------------------------------

Admin Menu Bar, Version 1.0
24 August 2006


Written by Ted Serbinski, aka, m3avrck
  hello@tedserbinski.com
  http://tedserbinski.com
  
  
Requirements: Drupal 4.7


--- INSTALLATION --------------------------------------------------------

1. Create a folder called "admin" in your theme directory and extract all
of the files to there


2. If you don't have already have one, create a template.php in your theme
directory and add the following code:

/**
 * Build the administrative menu shown to admins
 *
 * @return
 *  A string of HTML representing the admin menu
 */
function YOUR_THEME_NAME_admin_menu() {
  $output = '<ul id="admin-nav">';
  if (module_exist('devel')) {
    $output .= '<li class="expanded"><a href="#">devel</a><ul class="menu">';
    $output .= '<li class="leaf"><a href="'. url('admin/settings/devel') .'">module settings</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/cache/clear') .'">empty cache</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/phpinfo') .'">phpinfo()</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/menu/reset') .'">reset menus</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/reinstall') .'">reinstall modules</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/variable') .'">variable viewer</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/execute') .'">execute php code</a></li>';
    $output .= '<li class="leaf"><a href="'. url('devel/node_access/summary') .'">node access summary</a></li>';
    $output .= '</ul></li>';
  }
  $output .= menu_tree(1);
  $output .= '</ul>';
  
  return $output;
}


2. Open up page.tpl.php and somewhere between the <head> tags add:
  
   print theme('stylesheet_import', base_path() . path_to_theme() . '/admin/admin.css');
   print '<script type="text/javascript" src="'. base_path() . path_to_theme() .'/admin/admin.js"></script>';


3. Somewhere between the <body> add the following:
  
   <?php print YOUR_THEME_NAME_admin_menu(); ?>


4. Make sure you expand *all* menu items on admin > menu so they properly display

5. Refresh the page and enjoy the new admin menu bar!




--- TIPS ---------------------------------------------------------------

Pass an $admin variable to your page.tpl.php from template.php using
phptemplate_variables so you can hide the menu from non-admins.

/**
 * Intercept template variables
 *
 * @param $hook
 *   The name of the theme function being executed
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */
function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    case 'page':
      global $user;
      //you can easily extend this to test for other users and other roles
      if ($user->uid == 1) {
        $vars['admin'] = TRUE;
      }
    }
  }
}

Then in your page.tpl.php you can use the following:

if ($admin) {
  print theme('stylesheet_import', base_path() . path_to_theme() . '/admin/admin.css');
  print '<script type="text/javascript" src="'. base_path() . path_to_theme() .'/admin/admin.js"></script>';
}

And:

<?php if ($admin) print ted_admin_menu(); ?>